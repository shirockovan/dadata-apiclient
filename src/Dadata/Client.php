<?php

/* 
 *  Copyright 2016 Evgeniy Zhelyazkov <evgeniyzhelyazkov@gmail.com>.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * The Dadata API Client
 *
 * @author Evgeniy Zhelyazkovt <evgeniyzhelyazkov@gmail.com>
 */
class Dadata_Client extends Google_Client
{
    const LIBVER = "0.0.1-alfa";
    const USER_AGENT_SUFFIX = "dadata-api-php-client/";
    
  /**
   * Construct the Dadata Client.
   *
   * @param $config Dadata_Config or string for the ini file to load
   */
  public function __construct($config = null)
  {
    $this->config = new Dadata_Config();
    $this->config->setApiKey($config['api_key']);
    $this->config->setContentType($config['content_type']);
    parent::__construct($config); 
  }
  
    /**
     * Helper method to execute deferred HTTP requests.
     *
     * @return object of the type of the expected class or array.
     */
    public function execute($request)
    {
        if ($request instanceof Google_Http_Request) {
            $request->setUserAgent(
                $this->getApplicationName()
                . " " . self::USER_AGENT_SUFFIX
                . self::LIBVER
            );                      
            $io = $this->getIo();
            $request->setPostBody(json_encode($request->getQueryParams()));
            $httpRequest = $io->makeRequest($request);     
            $httpRequest->setExpectedClass($request->getExpectedClass());
            return Google_Http_REST::decodeHttpResponse($httpRequest);
        } else if ($request instanceof \Google_Http_Batch) {
            return $request->execute();
        } else {
            throw new Google_Exception("Do not know how to execute this type of object.");
        }
    }  
   
  /**
   * Set base URL to use for API calls
   * @param $uri string - the domain to use.
   */
  public function setBasePath($uri)
  {
    $this->config->setBasePath($uri);
  }

  /**
   * @return string the base URL to use for calls to the APIs
   */
  public function getBasePath()
  {
    return $this->config->getBasePath();
  }  

  /**
   * @return string the Api Key to use for calls to the APIs
   */
  public function getApiKey()
  {
    return $this->config->getApiKey();
  }
  
  /**
   * @return string the Content-Type to use for calls to the APIs
   */
  public function getContentType()
  {
    return $this->config->getContentType();
  }
  
  /**
   * Set Content-Type to use for API calls
   * @param $contentType string
   */
  public function setContentType($contentType)
  {
    $this->config->setContentType($contentType);
  }

}
